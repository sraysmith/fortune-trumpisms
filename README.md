# TRUMPISMS

**Trumpisms** according to multiple sources are negatively defined as false statements, lies, and outrageous statements made by Donald Trump. It is also used to describe the President's nontraditional political philosophy and to paint supporters as having a disorder.

Personally, I think the term ***"Trumpism"*** would be best served to describe Donald Trump's most inspirational quotes. I have compiled some of what I believe to be his most inspirational quotes into a database to be recalled using Fortune.

![Trumpism](img/trumpisms.png)

### **Dependencies**
fortune-mod

If you do not already have it just run sudo pacman -S fortune-mod to install it.

## **Instructions**
1. git clone this repository
2. cp trumpisms and trumpisms.dat to /usr/share/fortune
3. run fortune trumpisms in your terminal
4. Enjoy!!!

#### For more fun
You can pipe into cowsay and/or lolcat.
*"Make your __terminal__ great again!"*

fortune trumpisms | cowsay | lolcat

![Trumpism](img/trumpisms2.png)
